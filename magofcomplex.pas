program prog1_9(input,output);
const i=sqrt(-1);
var x,y,mag:real;
begin
writeln('Program to calculate magnitude of complex number');
writeln('Enter the values of x and y');
readln(x,y);
mag:=sqrt(x*x+y*y);
writeln('Magnitude of given complex number is',mag:8:2);
end.