program prog1_9(input,output);
const i=sqrt(-1);
const PI=3.1417;
var x,y,angle:real;
begin
writeln('Program to calculate angle of complex number');
writeln('Enter the values of x and y');
readln(x,y);
angle:=arctan(y/x)*PI/180.0;
writeln('Angle of given complex number is',angle:8:2,'radians');
end.