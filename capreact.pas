program prog1_7(input,output);
const PI=3.14157;
var freq,cap,X_c:real;
begin
writeln('Program to find capacitive reactance');
writeln('Enter the value of freq');
readln(freq);
writeln('Enter the value of capacitance');
readln(cap);
X_c:=1/(2*PI*freq*cap);
writeln('Capacitive reactance is',X_c:8:3,'ohms');
end.