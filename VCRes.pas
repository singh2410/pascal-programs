program prog1_3(input,output);
(*Resistance finding using Voltage and Current*)
var Voltage,Current,Resistance:real;
begin
    writeln('Enter Voltage and current');
    readln(Voltage,Current);
    Resistance:=Voltage/Current;
    writeln('Resistance is ',Resistance);
end.