program prog1_2(input,output);
var resistor1,resistor2,equ_resistance:real;
begin
    resistor1:=1e6;
    resistor2:=1e6;
    equ_resistance:=1/((1/resistor1)+(1/resistor2));
    writeln('Equivalent resistance is ',equ_resistance);
end.