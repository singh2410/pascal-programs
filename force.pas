program prog1_6(input,output);
const G=6.67e-11;
var force,m1,m2,dis:real;
begin
writeln('Enter the mass of first object');
readln(m1);
writeln('Enter the mass of second object');
readln(m2);
writeln('Enter the disatnce between two objects');
readln(dis);
force:=(G*m1*m2)/(dis*dis);
writeln('Force is',force:8:2);
end.