program prog1_5(input,output);
(*Gradient of line*)
var x1,x2,y1,y2,grad:real;
begin
writeln('Enter x1,y1 >> ');
readln(x1,y1);
writeln('Enter x2,y2 >> ');
readln(x2,y2);
grad:=(y2-y1)/(x2-x1);
writeln('Gradient of the line is',grad:8:2);
end.