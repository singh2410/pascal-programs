program prog1_8(input,output);
const PI=3.1417;
var x,y,z,angle:real;
begin
writeln('Program to find angle in right angled triangle');
writeln('Enter the value of x and y >>');
readln(x,y);
writeln('Enter the angle between them');
readln(angle);
z:=sqrt(x*x+y*y);
angle:=arctan(y/x)*180.0/PI;
writeln('Z is',z:8:2,'angle is',angle:8:2,'deg');
end.
